import React, { Component } from 'react';
import './App.css';
import Modal from "./UI/modal/modal";
import Button from "./UI/Button/Button";
import Wrapper from "./hoc/Wrapper";
import Alert from "./UI/Alert/Alert";

class App extends Component {
    state={
        modalShow: false,
        buttonShow: false,
        alertShow: false

    };


    modalShow = () => {
        this.setState({modalShow: true});
    };
    alertShow = () => {
        this.setState({alertShow: true});
    };

    modalHide = () => {
        this.setState({modalShow: false});
    };
    alertHide = () => {
        this.setState({alertShow: false});
    };
    btnShow = () => {
        this.setState({buttonShow: true});
    };



render() {
    let btn = null;
    if(this.state.buttonShow){
        btn = <Wrapper>
            <Button btnType="Danger" clicked={this.modalHide}>CANCEL</Button>
            <Button btnType="Success" clicked={this.alertShow}>CONTINUE</Button>
        </Wrapper>;
    }


    return (
      <div className="App">
          <button onClick={this.modalShow}>Show modal</button>
              <Modal
                  closed={this.modalHide}
                  show={this.state.modalShow}
                  title="Your content may be here"
              >

                 <p>Your content may be here</p>
                  <button onClick={this.btnShow} style={{display: !this.state.buttonShow ? 'block':'none'}}>Show buttons</button>
                  {btn}
                  <Alert
                      show={this.state.alertShow}
                      clickDismissable
                      dismiss={this.alertHide}
                      type="Warning"
                  ><p>This is a warning type alert</p>
                  </Alert>

              </Modal>

      </div>
    );
  }
}

export default App;
