import React from 'react';
import './Alert.css'
import Button from "../Button/Button";

const Alert = props => {
    return (
        <div
            className={['Alert', props.type].join(' ')}
            style={{display: props.show ? 'block' : 'none'}}
            onClick={props.clickDismissable ? props.dismiss : null}
        >
            {props.children}

            <Button show={props.clickDismissable} clicked={props.dismiss} btnType={props.type}>Close</Button>
        </div>
    )
};

export default Alert;