import React from 'react';
import './modal.css'
import Wrapper from "../../hoc/Wrapper";
import Backdrop from "../BackDrop/BackDrop";

const Modal = props=>(
    <Wrapper>
    <Backdrop show={props.show} clicked={props.closed}/>
    <div className='Modal' /*style={{display: props.show ? 'block':'none'}}*/ style={{
        transform: props.show ? 'translateY(0)' : 'translateY(-100vh)',
        opacity: props.show ? '1' : '0'

    }}>
        <h2 className="title">{props.title}</h2>
        <span onClick={props.closed} className="Close">X</span>
        {props.children}

    </div>
    </Wrapper>
);

export default Modal